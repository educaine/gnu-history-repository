# nothing below this line should need changing. If you absolutely have to,
# drop me a note including your changes. zmbenhal@netcom.com

# This work is copyrighted. See COPYRIGHT.OLD & COPYRIGHT.NEW for 
#  details. If they are missing then this copy is in violation of  
#  the copyright conditions.                                        

SRCS=	comp_main.c comp_hash.c comp_captab.c comp_scan.c comp_error.c \
	comp_parse.c read_entry.c dump.c names.c strerror.c \
	lib_setup.c lib_kernel.c lib_tparm.c lib_tputs.c lib_slk.c lib_pad.c\
	lib_unctrl.c lib_raw.c lib_vidattr.c lib_trace.c lib_ti.c lib_keyname.c\
	lib_beep.c lib_doupdate.c lib_refresh.c lib_initscr.c lib_newwin.c \
	lib_addch.c lib_addstr.c lib_scroll.c lib_clreol.c lib_touch.c \
	lib_mvcur.c lib_delwin.c lib_endwin.c lib_clrbot.c lib_move.c \
	lib_printw.c lib_scanw.c lib_erase.c lib_getch.c lib_options.c \
	lib_box.c lib_clear.c lib_delch.c lib_deleteln.c lib_insch.c \
	lib_insertln.c lib_getstr.c lib_mvwin.c lib_longname.c lib_tstp.c \
	lib_newterm.c lib_set_term.c lib_overlay.c lib_scrreg.c lib_color.c \
	lib_insstr.c lib_insdel.c lib_termcap.c lib_twait.c sigaction.c

# Normal, optimised object files
LIBOBJS= lib_setup.o lib_kernel.o lib_tparm.o read_entry.o lib_tputs.o lib_pad.o\
	lib_unctrl.o lib_raw.o lib_vidattr.o lib_trace.o lib_beep.o names.o\
	lib_doupdate.o lib_refresh.o lib_initscr.o lib_newwin.o lib_addch.o \
	lib_addstr.o lib_scroll.o lib_clreol.o lib_touch.o lib_mvcur.o lib_keyname.o\
	lib_delwin.o lib_endwin.o lib_clrbot.o lib_move.o lib_printw.o \
	lib_scanw.o lib_erase.o lib_getch.o lib_options.o lib_acs.o lib_slk.o\
	lib_box.o lib_clear.o lib_delch.o lib_deleteln.o lib_insch.o lib_ti.o\
	lib_insertln.o lib_getstr.o lib_mvwin.o lib_longname.o lib_tstp.o \
	lib_newterm.o lib_set_term.o lib_overlay.o lib_scrreg.o lib_color.o \
	lib_insstr.o lib_insdel.o lib_termcap.o lib_twait.o sigaction.o strerror.o

# Debugging object files
DLIBOBJS= lib_setup.d lib_kernel.d lib_tparm.d read_entry.d lib_tputs.d lib_pad.d\
	lib_unctrl.d lib_raw.d lib_vidattr.d lib_trace.d lib_beep.d names.d\
	lib_doupdate.d lib_refresh.d lib_initscr.d lib_newwin.d lib_addch.d \
	lib_addstr.d lib_scroll.d lib_clreol.d lib_touch.d lib_mvcur.d lib_keyname.d\
	lib_delwin.d lib_endwin.d lib_clrbot.d lib_move.d lib_printw.d \
	lib_scanw.d lib_erase.d lib_getch.d lib_options.d lib_acs.d lib_slk.d\
	lib_box.d lib_clear.d lib_delch.d lib_deleteln.d lib_insch.d lib_ti.d\
	lib_insertln.d lib_getstr.d lib_mvwin.d lib_longname.d lib_tstp.d \
	lib_newterm.d lib_set_term.d lib_overlay.d lib_scrreg.d lib_color.d \
	lib_insstr.d lib_insdel.d lib_termcap.d lib_twait.d sigaction.o strerror.d

# Profiled object files
PLIBOBJS= lib_setup.pg lib_kernel.pg lib_tparm.pg read_entry.pg lib_tputs.pg lib_pad.pg\
	lib_unctrl.pg lib_raw.pg lib_vidattr.pg lib_trace.pg lib_beep.pg names.pg\
	lib_doupdate.pg lib_refresh.pg lib_initscr.pg lib_newwin.pg lib_addch.pg \
	lib_addstr.pg lib_scroll.pg lib_clreol.pg lib_touch.pg lib_mvcur.pg lib_keyname.pg\
	lib_delwin.pg lib_endwin.pg lib_clrbot.pg lib_move.pg lib_printw.pg \
	lib_scanw.pg lib_erase.pg lib_getch.pg lib_options.pg lib_acs.pg lib_slk.pg\
	lib_box.pg lib_clear.pg lib_delch.pg lib_deleteln.pg lib_insch.pg lib_ti.pg\
	lib_insertln.pg lib_getstr.pg lib_mvwin.pg lib_longname.pg lib_tstp.pg \
	lib_newterm.pg lib_set_term.pg lib_overlay.pg lib_scrreg.pg lib_color.pg \
	lib_insstr.pg lib_insdel.pg lib_termcap.pg lib_twait.pg sigaction.pg strerror.pg

COMPOBJS= comp_main.o comp_hash.o comp_captab.o comp_scan.o comp_error.o \
	comp_parse.o read_entry.o

.SUFFIXES: .d .pg

.c.o:
	$(CC) -c $(CFLAGS) -o $*.o $*.c

.c.d:
	$(CC) -c $(DFLAGS) -o $*.d $*.c

.c.pg:
	$(CC) -c $(PFLAGS) -o $*.pg $*.c


all:	tic untic $(LIBS) 

install.all:	install.libs install.man install.data

$(INCLUDE) $(LIB) $(SRCDIR):
	mkdir -p $@

install.libs: terminfo.h $(LIBS) $(INCLUDE) $(LIB)
	sed -e 's/clude *"terminfo.h" *$$/clude <nterm.h>/' curses.h > ncurses.h 
	$(INSTALL) -c -m 0644 -o bin -g bin terminfo.h $(INCLUDE)/nterm.h
	$(INSTALL) -c -m 0644 -o bin -g bin ncurses.h $(INCLUDE)/ncurses.h
	$(INSTALL) -c -m 0644 -o bin -g bin unctrl.h $(INCLUDE)/unctrl.h
	$(INSTALL) -c -m 0644 -o bin -g bin termcap.h $(INCLUDE)/termcap.h
	@for lib in $(LIBS); do \
		echo installing $${lib} as $(LIB)/$${lib}; \
		$(INSTALL) -c -m 0644 -o bin -g bin $${lib} $(LIB)/$${lib}; \
		ranlib $(LIB)/$${lib}; \
	done

install.man:
	(cd ../man; make SRCDIR=$(SRCDIR) INSTALL=$(INSTALL) MAN=$(MAN) )

install.data: tic untic $(SRCDIR) 
	$(INSTALL) -c -s -m 755 -o bin -g bin tic $(SRCDIR)/tic
	$(INSTALL) -c -s -m 755 -o bin -g bin untic $(SRCDIR)/untic
	@for e in ../data/*; do \
		echo compiling $$e entry;\
		$(SRCDIR)/tic $$e;\
	done

alias: 
	(cd $(INCLUDE); rm term.h curses.h; ln nterm.h term.h; ln ncurses.h curses.h)

lib:	terminfo.h libncurses.a	
libncurses.a:	${LIBOBJS}
	ar rv libncurses.a $?
	ranlib libncurses.a

dlib:	terminfo.h libdcurses.a	
libdcurses.a:	${DLIBOBJS}
	ar rv libdcurses.a $?
	ranlib libdcurses.a

plib:	terminfo.h libpcurses.a	
libpcurses.a:	${PLIBOBJS}
	ar rv libpcurses.a $?
	ranlib libpcurses.a

tic: ${COMPOBJS}
	$(CC) -o tic ${COMPOBJS}

untic: dump.o names.o read_entry.o
	$(CC) -o untic dump.o names.o read_entry.o

keys.tries: keys.list
	awk -f MKkeys.awk keys.list > keys.tries

lib_options.o: keys.tries lib_options.c terminfo.h

terminfo.h: Caps MKterm.h.awk
	awk -f MKterm.h.awk Caps > terminfo.h

comp_captab.c: Caps MKcaptab.awk
	awk -f MKcaptab.awk Caps > comp_captab.c

comp_main.o: compiler.h terminfo.h
	$(CC) $(CFLAGS) -DSRCDIR=\"$(SRCDIR)\" -o comp_main.o -c comp_main.c

dump.o: dump.c terminfo.h compiler.h
	$(CC) $(CFLAGS) -DSRCDIR=\"$(SRCDIR)\" -o dump.o -c dump.c

lib_setup.o: lib_setup.c
	$(CC) $(CFLAGS) -DSRCDIR=\"$(SRCDIR)\" -o lib_setup.o -c lib_setup.c

lib_setup.d: lib_setup.c
	$(CC) $(DFLAGS) -DSRCDIR=\"$(SRCDIR)\" -o lib_setup.d -c lib_setup.c

lib_setup.pg: lib_setup.c
	$(CC) $(PFLAGS) -DSRCDIR=\"$(SRCDIR)\" -o lib_setup.pg -c lib_setup.c

lib_keyname.c: keys.list MKkeyname.awk
	awk -f MKkeyname.awk keys.list > lib_keyname.c

names.c: Caps MKnames.awk
	awk -f MKnames.awk Caps
	cat boolnames boolfnames boolcodes numnames numfnames numcodes strnames strfnames strcodes> names.c
	rm -f boolnames boolfnames boolcodes numnames numfnames numcodes strnames strfnames strcodes

clean:
	rm -f *.[aod] *.pg term.h comp_captab.c tags lib_keyname.c keys.tries names.c

clobber: clean
	rm -f tic untic Config.* ncurses.h terminfo.h Makefile 


