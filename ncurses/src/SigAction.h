
/* This work is copyrighted. See COPYRIGHT.OLD & COPYRIGHT.NEW for   *
   details. If they are missing then this copy is in violation of    *
   the copyright conditions.                                        */

/* This file exist to handle non-POSIX systems which don't
   have <unistd.h>, and usually no sigaction() nor
   <termios.h>
*/

#ifndef _SIGACTION_H
#define _SIGACTION_H

#include <libc.h>

typedef struct sigvec sigaction_t;

#define sa_mask sv_mask
#define sa_handler sv_handler
#define sa_flags sv_flags

extern void sigaction (int sig, sigaction_t * sigact, sigaction_t *  osigact);
extern void sigprocmask (int mode, int *mask, int *omask);
extern void sigemptyset (int *mask);
extern int sigsuspend (int *mask);
extern void sigdelset (int *mask, int sig);
extern void sigaddset (int *mask, int sig);

#endif

