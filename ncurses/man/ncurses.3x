.TH ncurses 3X ""
.ds n 5
.ds d @TERMINFO@
.SH NAME
\fBncurses\fR - CRT screen handling and optimization package
.SH SYNOPSIS
\fB#include <ncurses.h>\fR
.br
.SH DESCRIPTION
The \fBncurses\fR library routines give the user a terminal-independent
method of updating character screens with reasonable optimization.

The \fBncurses\fR routines emulate the \fBcurses\fR(3X) library of System V
Release 4 UNIX, but is freely redistributable in source form.  Differences from
the SVr4 curses are described in the BUGS sections of individual man pages.
There are only few of these, and as ncurses matures they will become fewer
still.

A program using these routines must be linked with the \fB-lncurses\fR option,
or (if they have been generated) with one of the debugging libraries
\fB-ldcurses\fR or \fB-lpcurses\fR.  The dcurses library generates trace logs
that describe curses actions; the pcurses library supports profiling.

The \fBncurses\fR package supports: overall screen, window and pad
manipulation; output to windows and pads; reading terminal input; control over
terminal and \fBncurses\fR input and output options; environment query
routines; color manipulation; use of soft label keys; terminfo capabilities;
and access to low-level \fBncurses\fR routines.

To initialize the routines, the routine \fBinitscr\fR or \fBnewterm\fR
must be called before any of the other routines that deal with windows
and screens are used.  The routine \fBendwin\fR must be called before
exiting.  To get character-at-a-time input without echoing (most
interactive, screen oriented programs want this), the following
sequence should be used:

      \fBinitscr(); cbreak(); noecho;\fR

Most programs would additionally use the sequence:

      \fBnonl(); intrflush(stdscr,FALSE); keypad(stdscr,TRUE);\fR

Before a \fBncurses\fR program is run, the tab stops of the terminal
should be set and its initialization strings, if defined, must be
output.  This can be done by executing the \fBtput init\fR command
after the shell environment variable \fBTERM\fR has been exported.
[See \fBterminfo\fR(\*n) for further details.]

The \fBncurses\fR library permits manipulation of data structures,
called \fIwindows\fR, which can be thought of as two-dimensional
arrays of characters representing all or part of a CRT screen.  A
default window called \fBstdscr\fR, which is the size of the terminal
screen, is supplied.  Others may be created with \fBnewwin\fR.

Windows are referred to by variables declared as \fBWINDOW *\fR.
These data structures are manipulated with routines described on 3X
pages (whose names begin "curs_").  Among which the most basic
routines are \fBmove\fR and \fBaddch\fR.  More general versions of
these routines are included with names beginning with \fBw\fR,
allowing the user to specify a window.  The routines not beginning
with \fBw\fR affect \fBstdscr\fR.)

After using routines to manipulate a window, \fBrefresh\fR is called,
telling \fBncurses\fR to make the user's CRT screen look like
\fBstdscr\fR.  The characters in a window are actually of type
\fBchtype\fR, (character and attribute data) so that other information
about the character may also be stored with each character.

Special windows called \fIpads\fR may also be manipulated.  These are windows
which are not constrained to the size of the screen and whose contents need not
be completely displayed.  See curs_pad(3X) for more information.

In addition to drawing characters on the screen, video attributes and colors
may be supported, causing the characters to show up in such modes as
underlined, in reverse video, or in color on terminals that support such
display enhancements.  Line drawing characters may be specified to be output.
On input, \fBncurses\fR is also able to translate arrow and function keys that
transmit escape sequences into single values.  The video attributes, line
drawing characters, and input values use names, defined in \fB<ncurses.h>\fR,
such as \fBA_REVERSE\fR, \fBACS_HLINE\fR, and \fBKEY_LEFT\fR.

If the environment variables \fBLINES\fR and \fBCOLUMNS\fR are set, or if the
program is executing in a window environment, line and column information in
the environment will override information read by \fIterminfo\fR.  This would
effect a program running in an AT&T 630 layer, for example, where the size of a
screen is changeable.

If the environment variable \fBTERMINFO\fR is defined, any program using
\fBncurses\fR checks for a local terminal definition before checking in the
standard place.  For example, if \fBTERM\fR is set to \fBatt4424\fR, then the
compiled terminal definition is found in

      \fB\*d/a/att4424\fR.

(The \fBa\fR is copied from the first letter of \fBatt4424\fR to avoid
creation of huge directories.)  However, if \fBTERMINFO\fR is set to
\fB$HOME/myterms\fR, \fBncurses\fR first checks

      \fB$HOME/myterms/a/att4424\fR,

and if that fails, it then checks

      \fB\*d/a/att4424\fR.

This is useful for developing experimental definitions or when write
permission in \fB\*d\fR is not available.

The integer variables \fBLINES\fR and \fBCOLS\fR are defined in
\fB<ncurses.h>\fR and will be filled in by \fBinitscr\fR with the size of the
screen.  The constants \fBTRUE\fR and \fBFALSE\fR have the values \fB1\fR and
\fB0\fR, respectively.

The \fBncurses\fR routines also define the \fBWINDOW *\fR variable \fBcurscr\fR
which is used for certain low-level operations like clearing and redrawing a
screen containing garbage.  The \fBcurscr\fR can be used in only a few
routines.

.SS Routine and Argument Names
Many \fBncurses\fR routines have two or more versions.  The routines prefixed
with \fBw\fR require a window argument.  The routines prefixed with \fBp\fR
require a pad argument.  Those without a prefix generally use \fBstdscr\fR.

The routines prefixed with \fBmv\fR require a \fIy\fR and \fIx\fR
coordinate to move to before performing the appropriate action.  The
\fBmv\fR routines imply a call to \fBmove\fR before the call to the
other routine.  The coordinate \fIy\fR always refers to the row (of
the window), and \fIx\fR always refers to the column.  The upper
left-hand corner is always (0,0), not (1,1).

The routines prefixed with \fBmvw\fR take both a window argument and
\fIx\fR and \fIy\fR coordinates.  The window argument is always
specified before the coordinates.

In each case, \fIwin\fR is the window affected, and \fIpad\fR is the
pad affected; \fIwin\fR and \fIpad\fR are always pointers to type
\fBWINDOW\fR.

Option setting routines require a Boolean flag \fIbf\fR with the value
\fBTRUE\fR or \fBFALSE\fR; \fIbf\fR is always of type \fBbool\fR.  The
variables \fIch\fR and \fIattrs\fR below are always of type
\fBchtype\fR.  The types \fBWINDOW\fR, \fBSCREEN\fR, \fBbool\fR, and
\fBchtype\fR are defined in \fB<ncurses.h>\fR.  The type \fBTERMINAL\fR
is defined in \fB<term.h>\fR.  All other arguments are integers.

.SS Routine Name Index
The following table lists each \fBncurses\fR routine and the name of
the manual page on which it is described.

.nf 
\fBncurses\fR Routine Name    Manual Page Name
___________________________________________
addch                  curs_addch(3X)
addchnstr              curs_addchstr(3X)
addchstr               curs_addchstr(3X)
addnstr                curs_addstr(3X)
addstr                 curs_addstr(3X)
attroff                curs_attr(3X)
attron                 curs_attr(3X)
attrset                curs_attr(3X)
baudrate               curs_termattrs(3X)
beep                   curs_beep(3X)
bkgd                   curs_bkgd(3X)
bkgdset                curs_bkgd(3X)
border                 curs_border(3X)
box                    curs_border(3X)
can_change_color       curs_color(3X)
cbreak                 curs_inopts(3X)
clear                  curs_clear(3X)
clearok                curs_outopts(3X)
clrtobot               curs_clear(3X)
clrtoeol               curs_clear(3X)
color_content          curs_color(3X)
copywin                curs_overlay(3X)
curs_set               curs_kernel(3X)
def_prog_mode          curs_kernel(3X)
def_shell_mode         curs_kernel(3X)
del_curterm            curs_terminfo(\*n)
delay_output           curs_util(3X)
delch                  curs_delch(3X)
deleteln               curs_deleteln(3X)
delscreen              curs_initscr(3X)
delwin                 curs_window(3X)
derwin                 curs_window(3X)
doupdate               curs_refresh(3X)
dupwin                 curs_window(3X)
echo                   curs_inopts(3X)
echochar               curs_addch(3X)
endwin                 curs_initscr(3X)
erase                  curs_clear(3X)
erasechar              curs_termattrs(3X)
filter                 curs_util(3X)
flash                  curs_beep(3X)
flushinp               curs_util(3X)
getbegyx               curs_getyx(3X)
getch                  curs_getch(3X)
getmaxyx               curs_getyx(3X)
getparyx               curs_getyx(3X)
getstr                 curs_getstr(3X)
getsyx                 curs_kernel(3X)
getwin                 curs_util(3X)
getyx                  curs_getyx(3X)
halfdelay              curs_inopts(3X)
has_colors             curs_color(3X)
has_ic                 curs_termattrs(3X)
has_il                 curs_termattrs(3X)
hline                  curs_border(3X)
idcok                  curs_outopts(3X)
idlok                  curs_outopts(3X)
immedok                curs_outopts(3X)
inch                   curs_inch(3X)
inchnstr               curs_inchstr(3X)
inchstr                curs_inchstr(3X)
init_color             curs_color(3X)
init_pair              curs_color(3X)
initscr                curs_initscr(3X)
innstr                 curs_instr(3X)
insch                  curs_insch(3X)
insdelln               curs_deleteln(3X)
insertln               curs_deleteln(3X)
insnstr                curs_insstr(3X)
insstr                 curs_insstr(3X)
instr                  curs_instr(3X)
intrflush              curs_inopts(3X)
is_linetouched         curs_touch(3X)
is_wintouched          curs_touch(3X)
isendwin               curs_initscr(3X)
keyname                curs_util(3X)
keypad                 curs_inopts(3X)
killchar               curs_termattrs(3X)
leaveok                curs_outopts(3X)
longname               curs_termattrs(3X)
meta                   curs_inopts(3X)
move                   curs_move(3X)
mvaddch                curs_addch(3X)
mvaddchnstr            curs_addchstr(3X)
mvaddchstr             curs_addchstr(3X)
mvaddnstr              curs_addstr(3X)
mvaddstr               curs_addstr(3X)
mvcur                  curs_terminfo(\*n)
mvdelch                curs_delch(3X)
mvderwin               curs_window(3X)
mvgetch                curs_getch(3X)
mvgetstr               curs_getstr(3X)
mvinch                 curs_inch(3X)
mvinchnstr             curs_inchstr(3X)
mvinchstr              curs_inchstr(3X)
mvinnstr               curs_instr(3X)
mvinsch                curs_insch(3X)
mvinsnstr              curs_insstr(3X)
mvinsstr               curs_insstr(3X)
mvinstr                curs_instr(3X)
mvprintw               curs_printw(3X)
mvscanw                curs_scanw(3X)
mvwaddch               curs_addch(3X)
mvwaddchnstr           curs_addchstr(3X)
mvwaddchstr            curs_addchstr(3X)
mvwaddnstr             curs_addstr(3X)
mvwaddstr              curs_addstr(3X)
mvwdelch               curs_delch(3X)
mvwgetch               curs_getch(3X)
mvwgetstr              curs_getstr(3X)
mvwin                  curs_window(3X)
mvwinch                curs_inch(3X)
mvwinchnstr            curs_inchstr(3X)
mvwinchstr             curs_inchstr(3X)
mvwinnstr              curs_instr(3X)
mvwinsch               curs_insch(3X)
mvwinsnstr             curs_insstr(3X)
mvwinsstr              curs_insstr(3X)
mvwinstr               curs_instr(3X)
mvwprintw              curs_printw(3X)
mvwscanw               curs_scanw(3X)
napms                  curs_kernel(3X)
newpad                 curs_pad(3X)
newterm                curs_initscr(3X)
newwin                 curs_window(3X)
nl                     curs_outopts(3X)
nocbreak               curs_inopts(3X)
nodelay                curs_inopts(3X)
noecho                 curs_inopts(3X)
nonl                   curs_outopts(3X)
noqiflush              curs_inopts(3X)
noraw                  curs_inopts(3X)
notimeout              curs_inopts(3X)
overlay                curs_overlay(3X)
overwrite              curs_overlay(3X)
pair_content           curs_color(3X)
pechochar              curs_pad(3X)
pnoutrefresh           curs_pad(3X)
prefresh               curs_pad(3X)
printw                 curs_printw(3X)
putp                   curs_terminfo(\*n)
putwin                 curs_util(3X)
qiflush                curs_inopts(3X)
raw                    curs_inopts(3X)
redrawwin              curs_refresh(3X)
refresh                curs_refresh(3X)
reset_prog_mode        curs_kernel(3X)
reset_shell_mode       curs_kernel(3X)
resetty                curs_kernel(3X)
restartterm            curs_terminfo(\*n)
ripoffline             curs_kernel(3X)
savetty                curs_kernel(3X)
scanw                  curs_scanw(3X)
scr_dump               curs_scr_dump(3X)
scr_init               curs_scr_dump(3X)
scr_restore            curs_scr_dump(3X)
scr_set                curs_scr_dump(3X)
scrl                   curs_scroll(3X)
scroll                 curs_scroll(3X)
scrollok               curs_outopts(3X)
set_curterm            curs_terminfo(\*n)
set_term               curs_initscr(3X)
setscrreg              curs_outopts(3X)
setsyx                 curs_kernel(3X)
setterm                curs_terminfo(\*n)
setupterm              curs_terminfo(\*n)
slk_attroff            curs_slk(3X)
slk_attron             curs_slk(3X)
slk_attrset            curs_slk(3X)
slk_clear              curs_slk(3X)
slk_init               curs_slk(3X)
slk_label              curs_slk(3X)
slk_noutrefresh        curs_slk(3X)
slk_refresh            curs_slk(3X)
slk_restore            curs_slk(3X)
slk_set                curs_slk(3X)
slk_touch              curs_slk(3X)
standend               curs_attr(3X)
standout               curs_attr(3X)
start_color            curs_color(3X)
subpad                 curs_pad(3X)
subwin                 curs_window(3X)
syncok                 curs_window(3X)
termattrs              curs_termattrs(3X)
termname               curs_termattrs(3X)
tgetent                curs_termcap(3X)
tgetflag               curs_termcap(3X)
tgetnum                curs_termcap(3X)
tgetstr                curs_termcap(3X)
tgoto                  curs_termcap(3X)
tigetflag              curs_terminfo(\*n)
tigetnum               curs_terminfo(\*n)
tigetstr               curs_terminfo(\*n)
timeout                curs_inopts(3X)
touchline              curs_touch(3X)
touchwin               curs_touch(3X)
tparm                  curs_terminfo(\*n)
tputs                  curs_termcap(3X)
tputs                  curs_terminfo(\*n)
typeahead              curs_inopts(3X)
unctrl                 curs_util(3X)
ungetch                curs_getch(3X)
untouchwin             curs_touch(3X)
use_env                curs_util(3X)
vidattr                curs_terminfo(\*n)
vidputs                curs_terminfo(\*n)
vline                  curs_border(3X)
vwprintw               curs_printw(3X)
vwscanw                curs_scanw(3X)
waddch                 curs_addch(3X)
waddchnstr             curs_addchstr(3X)
waddchstr              curs_addchstr(3X)
waddnstr               curs_addstr(3X)
waddstr                curs_addstr(3X)
wattroff               curs_attr(3X)
wattron                curs_attr(3X)
wattrset               curs_attr(3X)
wbkgd                  curs_bkgd(3X)
wbkgdset               curs_bkgd(3X)
wborder                curs_border(3X)
wclear                 curs_clear(3X)
wclrtobot              curs_clear(3X)
wclrtoeol              curs_clear(3X)
wcursyncup             curs_window(3X)
wdelch                 curs_delch(3X)
wdeleteln              curs_deleteln(3X)
wechochar              curs_addch(3X)
werase                 curs_clear(3X)
wgetch                 curs_getch(3X)
wgetnstr               curs_getstr(3X)
wgetstr                curs_getstr(3X)
whline                 curs_border(3X)
winch                  curs_inch(3X)
winchnstr              curs_inchstr(3X)
winchstr               curs_inchstr(3X)
winnstr                curs_instr(3X)
winsch                 curs_insch(3X)
winsdelln              curs_deleteln(3X)
winsertln              curs_deleteln(3X)
winsnstr               curs_insstr(3X)
winsstr                curs_insstr(3X)
winstr                 curs_instr(3X)
wmove                  curs_move(3X)
wnoutrefresh           curs_refresh(3X)
wprintw                curs_printw(3X)
wredrawln              curs_refresh(3X)
wrefresh               curs_refresh(3X)
wscanw                 curs_scanw(3X)
wscrl                  curs_scroll(3X)
wsetscrreg             curs_outopts(3X)
wstandend              curs_attr(3X)
wstandout              curs_attr(3X)
wsyncdown              curs_window(3X)
wsyncup                curs_window(3X)
wtimeout               curs_inopts(3X)
wtouchln               curs_touch(3X)
wvline                 curs_border(3X)
.fi
.SH RETURN VALUE
Routines that return an integer return \fBERR\fR upon failure and an
integer value other than \fBERR\fR upon successful completion, unless
otherwise noted in the routine descriptions.

All macros return the value of the \fBw\fR version, except \fBsetscrreg\fR,
\fBwsetscrreg\fR, \fBgetyx\fR, \fBgetbegyx\fR, \fBgetmaxyx\fR.  The return
values of \fBsetscrreg\fR, \fBwsetscrreg\fR, \fBgetyx\fR, \fBgetbegyx\fR, and
\fBgetmaxyx\fR are undefined (\fIi\fR.\fIe\fR., these should not be used as the
right-hand side of assignment statements).

Routines that return pointers return \fBNULL\fR on error.
.SH SEE ALSO
\fBterminfo\fR(\*n) and 3X pages whose names begin "curs_" for detailed routine
descriptions.
.SH NOTES
The header file \fB<ncurses.h>\fR automatically includes the header files
\fB<stdio.h>\fR and \fB<unctrl.h>\fR.
.\"#
.\"# The following sets edit modes for GNU EMACS
.\"# Local Variables:
.\"# mode:nroff
.\"# fill-column:79
.\"# End:
