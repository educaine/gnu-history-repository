This is the latest release of ncurses (1.8.5)

PLEASE: before you send patches check with me. There are
several things that I'm aware of but I want to get people's
reaction before I fix them.

Ncurses has gone through major changes, most importantly:

1) The order of capabilties has been changed in order to achieve
binary compatibility with SVR4 terminfo database. This has the
unfortunate effect of breaking application currently linked with
ncurses. To ensure correct behavior, recompile all such programs.
Most programs using color or newer capabilities will break, others
will probably continue to work ok.

2) Pavel Curtis has renounced his copyright to the public domain.
This means that his original sources (posted to comp.sources.unix)
are now in the public domain. The current sources are NOT in the
public domian, they are copyrighted by me. I'm entertaining ideas
on what the new terms ncurses is released under.

3) Eric S. Raymond has supplied a complete set of man pages for
ncurses in ?roff format. They will eventually replace most of the 
current docs. Both sets are included in this release.

Changes from 1.8.3:
*) some of these changes were made earlier and made available as
   patches.
*) serious bugs in updating screen, which caused irratic non-display
   fixed.
*) fixed intialization for getch() related variable which cause
   unpredictable results.
*) fixed another doupdate bug which only appeared if you have
   parm_char.
*) implemented redrawln() and redrawwin().
*) implemented winsnstr() and related functions.
*) cleaned up insertln() and deleteln() and implemented (w)insdeln().
*) changed Makefile.dist so that installation of man pages will
   take note of the terminfo directory.
*) fixed Configure (removed the mysterious 'X').
*) Eric S. Raymond fixed the script.* files so that they work with
   stock awk.
*) He still seems to be having trouble getting things displayed on
   the screen. If anybody else has similar problems let me know.


Other changes and notes from 1.8.2 include:
*) SIGSEGV during scrolling no longer occurs.
*) Other problems with scrolling and use of idl have been corrected.
*) lib_getch.c has been re-written and should perform flawlessly.
   please use test/getch.c and any other programs to test this.
*) ripoffline() is implemented (Thanks to Eric) and slk_ functions
   changed accordingly.
*) I've added support for terminals that scroll if you write in the
   bottom-right corner.
*) fixed more bugs in pads code. If anybody has a program that uses
   pads I'd love a copy.
*) correct handling for terminal with back_color_erase capability
   (such as Linux console, and most PC terminals)
*) ^Z handling apparently didn't work (I should never trust code
   sent me to me without extensive testing). It now seems to be
   fixed. Let me know if you have problems.
*) I've added support for Apollo and NeXT, but it may still be
   incomplete, especially when dealing with the lack of POSIX
   features.
*) scrolling should be more effecient on terminals with idl
   capabilities. Please see src/lib_scroll.c for more notes.
*) The line drawing routines were offset by 1 at both ends. This
   is now fixed.
*) added a few missing prototypes and macros (eg. setterm())
*) fixed code in src/lib_overlay.c which used to crash.
*) added a few more programs in test/ The ones from the PDCurses
   package are useful, especially if you have SVR4 proper. I'm
   interested in the results you get on such a systems (Eric? ;-).
   They already exposed certain bugs in ncurses.
*) See src/README for porting notes.
*) The C++ code should really replace ncurses.h instead of working
   around it. It should avoid name-space clashes with nterm.h (use
   rows instead of lines, etc.)
*) The C++ should compile ok. I've added explicit rules to the
   Makefile because no C++ defaults are documented on the suns.
*) The docs say that echo() and nocbreak() are mutually exclusive.
   At the moment ncurses will switch to cbreak() if the case above
   occurs. Should it continue to do so? How about echo() and noraw()?
*) PDCurses seem to assume that wclear() will use current attribute
   when clearing the screen. According to Eric this is not the case
   with SVR4.
*) I have discovered, to my chagrin, the sun (and probably other
   systems) doesn't have strerror or vsscanf and God knows what else!
   I've added a strerror() and will do a vsscanf().
*) I've also found out that the src/script.* rely on gawk and will not
   work with stock awk or even with nawk. Any changes are welcome.
*) Linux is more tolerant of NULL dereferences than most systems. This
   fact was exposed by hanoi. 
*) ncurses still seems ineffecient in drawing the screen on a serial
   link between Linux and suns. The padding may be the culprit.
*) There seems to be one lingering problem with doupdate() after shelling
   out. Despite the fact the it is sending out the correct information
   to the terminal, nothing takes effect until you press ^L or another
   refresh takes place. And yes, output does get flushed.
*) FILL IN THE BLANK.

