#include <stdio.h>
#include <ncurses.h>
#include <nterm.h>

int
main()
{
int i;
char *s;

	setupterm(NULL, 1, (int *)0);
	fprintf(stderr, "SIZE %d x %d\n", columns, lines);
	fprintf(stderr, "BOOLCOUNT = %d\n", BOOLCOUNT);
	for (i = 0; i < BOOLCOUNT; i++) {
		s = boolnames[i];
		fprintf(stderr, "%-10s , %-32s 'boolnames[%d]' is %d\n", s, boolfnames[i], i, tigetflag(s));
	}
	fprintf(stderr, "NUMCOUNT = %d\n", NUMCOUNT);
	for (i = 0; i < NUMCOUNT; i++) {
		s = numnames[i];
		fprintf(stderr, "%-10s , %-32s 'numnames[%d]' is %d\n", s, numfnames[i], i, tigetnum(s));
	}
	fprintf(stderr, "STRCOUNT = %d\n", STRCOUNT);
	for (i = 0; i < STRCOUNT; i++) {
		s = strnames[i];
		fprintf(stderr, "%-10s , %-32s 'strnames[%d]' is %s\n", s, strfnames[i], i, tigetstr(s));
	}
	exit(0);
}
